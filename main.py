from PIL import Image
import os

def cropandresize(img, path, final_width, final_height):
    width, height = img.size # размер картинки
    watermark = '_@stockwalls.jpg'

    # print(width, height)

    if width < final_width or height < final_height:
        print("качество фото низкое для разрешения "+ final_width + "x" + final_height)
    else:
        if (width/height)>(final_width/final_height):
            new_height = height
            new_width = int(new_height * final_width / final_height)
            cropped = img.crop(((width-new_width)/2, 0, (width-new_width)/2+new_width, new_height))
            resized = cropped.resize((final_width, final_height))
            resized.save(path +str(final_width) +'x'+ str(final_height) + watermark)
        else:
            new_width = width
            new_height = int(new_width * final_height / final_width)
            cropped = img.crop((0, (height-new_height)/2, new_width, (height-new_height)/2+new_height))
            resized = cropped.resize((final_width, final_height))
            resized.save(path +str(final_width) +'x'+ str(final_height) + watermark)


def cropperauto(img, path):
    cropandresize(img, path, 1080, 1920)
    cropandresize(img, path, 1080, 2340)
    cropandresize(img, path, 1080, 2412)
    cropandresize(img, path, 1440, 2880)
    cropandresize(img, path, 1920, 1080)
    cropandresize(img, path, 3840, 2160)

if __name__ == "__main__":
    if not os.path.exists("input"):
        os.mkdir("input")
    if not os.path.exists("output"):
        os.mkdir("output")
    files = os.listdir("input")
    for file in files:
        try:
            img = Image.open("input/"+file)
            if not os.path.exists('output/'+file):
                os.mkdir('output/'+file)
            path = 'output/'+file + "/"
            cropperauto(img, path)
        except:
            print('Ошибка с файлом ' + file)
